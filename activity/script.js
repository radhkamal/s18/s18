// Real World Application of Objects

/*
	-Scenario:

		1. We would like to create a new game that would have several pokemon interact with each other

		2. Every pokemon should have the same set of properties and functions
	
*/

function Pokemon(name, level) {

	//properties
	this.name = name;
	this.level = level;
	this.health = 100;
	this.attack = level;

	//methods
	this.faint = function() {
		console.log(this.name + ' fainted.');
	},

	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name + ".");
		
		if (target.health >= 5) {
			console.log(target.name + '\'s health is now reduced to ' + (target.health -= this.attack) + ".");
		} else {
			target.faint();
		}
	}

}

// Creates a new instance of the "Pokemon" object each with their unique properties

let raichu = new Pokemon('Raichu', 16);
let butterfree = new Pokemon('Butterfree', 8);

console.log(raichu.name + "'s initial health is " + raichu.health + ".");
console.log(butterfree.name + "'s initial health is " + butterfree.health + ".");

raichu.tackle(butterfree);
raichu.tackle(butterfree);
raichu.tackle(butterfree);
raichu.tackle(butterfree);
raichu.tackle(butterfree);
raichu.tackle(butterfree);
raichu.tackle(butterfree);


/*

butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);
butterfree.tackle(raichu);

*/

